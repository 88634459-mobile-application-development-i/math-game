import 'package:flutter/material.dart';
import 'package:mathgame/constant.dart';

class numpad extends StatelessWidget {

  final String child;
  final VoidCallback onTap;
  var btnColor = Colors.amber;

  numpad({Key? key, required this.child, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    if ( child == 'C') {
      btnColor = Colors.green;
    } else if ( child == 'DEL') {
      btnColor = Colors.red;
    } else if ( child == 'OK' ) {
      btnColor = Colors.orange;
    }

    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: btnColor,
            borderRadius: BorderRadius.circular(4)
          ),
          child: Center(
              child: Text(
            child,
            style: indigoTextStyle,
          )),
        ),
      ),
    );
  }
}
