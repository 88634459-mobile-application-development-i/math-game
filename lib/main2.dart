import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mathgame/constant.dart';
import 'package:mathgame/util/message_dialog.dart';
import 'package:provider/provider.dart';
import 'calculator_class.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) => runApp(
    ChangeNotifierProvider(
      create: (BuildContext context) => Calculator(),
      child: const MaterialApp(
        home: MyApp(),
        debugShowCheckedModeBanner: false,
      ),
    ),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final counter = Provider.of<Calculator>(_);
  late Calculator _calculator;
  int numberA = 1;
  int numberB = 1;

  int questionLowerLimit = 0;
  int questionUpperLimit = 10;
  int level = 1;

  String answer = '?';
  final _textController = TextEditingController();
  late Timer _timer;
  int _timeout = 30;

  @override
  void initState() {
    super.initState();
    _calculator = context.read<Calculator>();
  }

  void _startTimer() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        _timeout--;
        if (_timeout == 0) {
          _timer.cancel();
          showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) {
                return MessageDialog(
                    message: "Time out!",
                    onTap: resetGame,
                    icon: Icons.rotate_left);
              });
        }
      });
    });
  }

  @override
  void dispose() {
    _textController.dispose();
    _timer.cancel();
    super.dispose();
  }

  void setAnswer() {
    if (_textController.text != '') {
      answer = _textController.text;
      checkResult();
    } else {
      showDialog(
          context: context,
          builder: (context) {
            return MessageDialog(
                message: "Answer is empty!",
                onTap: goBackToQuestion,
                icon: Icons.rotate_left);
          });
    }
  }

  void resetGame() {
    Navigator.of(context).pop(); // Pop dialog
    setState(() {
      _timeout = 30;
      questionUpperLimit = 10;
      level = 1;
      numberA = 1;
      numberB = 1;
      answer = '?';
      _startTimer();
    });
  }

  void checkResult() {
    if (numberA + numberB == int.parse(answer)) {
      level++;
      if (level % 5 == 0) {
        questionUpperLimit += 5;
      }
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) {
            return MessageDialog(
                message: "Correct!",
                onTap: goToNextQuestion,
                icon: Icons.arrow_forward);
          });
    } else {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) {
            return MessageDialog(
                message: "Incorrect!",
                onTap: goBackToQuestion,
                icon: Icons.rotate_left);
          });
      setState(() {
        answer = '?';
      });
    }
  }

  var randomNumber = Random();

  void goToNextQuestion() {
    Navigator.of(context).pop(); // Pop dialog
    _timeout = 30;
    _textController.clear();
    setState(() {
      answer = '?';
    });

    numberA = randomNumber.nextInt(questionUpperLimit);
    numberB = randomNumber.nextInt(questionUpperLimit);
  }

  void goBackToQuestion() {
    Navigator.of(context).pop(); // Pop dialog
    _textController.clear();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final counter = Provider.of<Calculator>(context);
    final Size size = MediaQuery.of(context).size;
    final double screenWidth = size.width;
    final double screenHeight = size.height;

    print("Hight " + screenHeight.toString());
    print("Width " + screenWidth.toString());
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // 20 % of the screen
              height: (screenHeight / 100) * 5,
              color: Colors.white,
              child: Center(
                  child: Text(
                'Simple Math Game',
                style: indigoTextStyle,
              )),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 60),
              child: SizedBox(
                width: screenWidth * 0.8,
                height: screenWidth * 0.8,
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    CircularProgressIndicator(
                      value: 1 - _timeout / 30,
                      valueColor: AlwaysStoppedAnimation(Colors.indigoAccent),
                      backgroundColor: Colors.indigo,
                      strokeWidth: 12,
                    ),
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 40),
                            child: Text(
                              '$_timeout',
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 50,
                                  fontFamily: 'Nexa',
                                  color: Colors.indigo),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text('${counter.firstValue} + ${counter.secondValue}',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 65,
                                        color: Colors.indigo,
                                        fontFamily: 'Nexa'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: SizedBox(
                width: (screenWidth / 100) * 60,
                child: TextField(
                  controller: _textController,
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  onSubmitted: (value) {
                    setAnswer();
                  },
                  decoration: InputDecoration(
                    hintText: 'Input your answer...',
                    hintStyle: indigoItalicTextStyle,
                    filled: true,
                    fillColor: Colors.white,
                    border: const OutlineInputBorder(),
                    suffixIcon: Padding(
                      padding: const EdgeInsets.all(6.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.indigo,
                          borderRadius: BorderRadius.circular(3),
                        ),
                        child: IconButton(
                          onPressed: setAnswer,
                          icon: const Icon(Icons.arrow_forward),
                          color: Colors.white,
                        ),
                      ),
                    ),
                    // suffixIcon: IconButton(
                    //   onPressed: setAnswer,
                    //   icon: const Icon(Icons.arrow_forward),
                    // ),
                  ),
                ),
              ),
            ),
            // Expanded(
            //   child: Container(
            //     child: Center(
            //       child: Row(
            //         mainAxisAlignment: MainAxisAlignment.center,
            //         children: [
            //           Text(
            //             numberA.toString() + '  +  ' + numberB.toString() + '  =  ',
            //             style: indigoTextStyle,
            //           ),
            //           Container(
            //             height: 50,
            //             width: 100,
            //             decoration: BoxDecoration(
            //               borderRadius: BorderRadius.circular(4),
            //               color: Colors.indigo,
            //             ),
            //             child: Center(
            //                 child: Text(
            //               answer,
            //               style: whiteTextStyle,
            //             )),
            //           )
            //         ],
            //       ),
            //     ),
            //   ),
            // ),,
          ],
        ),

    );
  }
}
