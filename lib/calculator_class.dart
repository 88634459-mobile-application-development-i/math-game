import 'dart:async';
import 'dart:math';

import 'package:flutter/foundation.dart';

enum Operation { plus, minus, multiply }

class Calculator with ChangeNotifier {
  late int firstValue;
  late int secondValue;
  late int result;
  late Operation operation;
  String operator = "";
  String status = "";
  late Timer _timer;
  late int _countdown;

  Calculator() {
    reset();
  }

  int get countdown => _countdown;

  void reset() {
    // Generate random values between 1 and 5
    firstValue = Random().nextInt(5) + 1;
    secondValue = Random().nextInt(5) + 1;

    // Generate random operation
    operation = Operation.values[Random().nextInt(Operation.values.length)];

    // Calculate result based on random operation
    switch (operation) {
      case Operation.plus:
        result = firstValue + secondValue;
        operator = "+";
        break;
      case Operation.minus:
        result = firstValue - secondValue;
        operator = "-";
        break;
      case Operation.multiply:
        result = firstValue * secondValue;
        operator = "*";
        break;
    }

    // Reset the countdown
    _countdown = 20;
    _startCountdown();
  }

  void _startCountdown() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (_countdown > 0) {
        _countdown--;
      } else {
        // Time's up! Reset the question.
        notifyListeners();
      }
    });
  }

  bool checkResult(int result) {
    bool isCorrect = this.result == result;
    if (isCorrect) {
      reset();
      notifyListeners();
    } else {
      // Decrement the countdown
      _countdown--;
      if (_countdown == 0) {
        // Time's up! Reset the question.
      }
      notifyListeners();
    }
    return isCorrect;
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}