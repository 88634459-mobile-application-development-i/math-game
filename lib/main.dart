import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:async';

import 'calculator_class.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: ChangeNotifierProvider<Calculator>(
        // it will not redraw the whole widget tree anymore
        create: (BuildContext context) => Calculator(),
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final resultController = TextEditingController();
  late Timer _timer;
  int _start = 10;
  bool _gameStarted = false;
  bool _timerRunning = false;

  @override
  void initState() {
    super.initState();
  }

  void startGame() {
    setState(() {
      _gameStarted = true;
    });
    startTimer();
  }

  void restart() {
    Navigator.of(context).pop();
    setState(() {
      _start = 10;
      _gameStarted = false;
      _timerRunning = false;
    });
    _timer.cancel();
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) {
        setState(() {
          if (_start < 1) {
            timer.cancel();
            _timerRunning = false;
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (_) {
                return AlertDialog(
                  title: Text('Time\'s up!'),
                  content: Text('You didn\'t answer in time.'),
                  actions: [
                    TextButton(
                      onPressed: () => restart(),
                      child: Text('RESTART'),
                    ),
                  ],
                );
              },
            );
          } else {
            _start = _start - 1;
          }
        });
      },
    );
    _timerRunning = true;
  }

  void resetTimer() {
    setState(() {
      _start = 10;
      _gameStarted = true;
      _timerRunning = true;
    });
    _timer.cancel();
    startTimer();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final calculator = Provider.of<Calculator>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Simple Math Game'),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              if (!_gameStarted) ...[
                Text(
                  'Welcome to Simple Math Game!',
                  style: TextStyle(fontSize: 20.0),
                ),
                SizedBox(
                  height: 20.0,
                ),
                ElevatedButton(
                  onPressed: () {
                    startGame();
                    calculator.reset();
                  },
                  child: Text(
                    'Start',
                    style: TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ] else ...[
                Text(
                  '${calculator.firstValue} ${calculator.operator} ${calculator.secondValue} = ??',style: TextStyle(fontSize: 30),
                ),
                SizedBox(
                  height: 20.0,
                ),
                SizedBox(
                width: 150,
                height: 150,
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      CircularProgressIndicator(
                        value: 1 - _start / 10,
                        valueColor: AlwaysStoppedAnimation(Colors.white),
                        backgroundColor: _start > 4 ? Colors.indigo : Colors.red,
                        strokeWidth: 10,
                      ),
                      Center(
                        child: Text(
                          '$_start',
                          style: TextStyle(fontSize: 35.0 , color: _start > 4 ? Colors.indigo : Colors.red , fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                SizedBox(
                  width: 300,
                  child: TextField(
                    controller: resultController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      hintText: 'Enter result',
                    ),
                    enabled: _timerRunning,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                ElevatedButton(
                  onPressed: _timerRunning
                      ? () {
                    final inputResult =
                        int.tryParse(resultController.text) ?? 0;
                    final isCorrect =
                    calculator.checkResult(inputResult);
                    resultController.text = '';
                    if (isCorrect) {
                      resetTimer();
                    }
                    showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (_) {
                        return AlertDialog(
                          title: Text(
                            isCorrect
                                ? 'Congratulations!'
                                : 'Oops!',
                          ),
                          content: Text(
                            isCorrect
                                ? 'Your result is correct!'
                                : 'Your result is incorrect!',
                          ),
                          actions: [
                            TextButton(
                              onPressed: () => Navigator.of(context).pop(),
                              child: Text('OK'),
                            ),
                          ],
                        );
                      },
                    );
                  } : null,
                  child: Text(
                    'Send',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ]),
      ),
    );
  }
}
