import 'package:flutter/material.dart';

var indigoTextStyle = const TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 32,
    color: Colors.indigo,
    fontFamily: 'Nexa');

var blackTextStyle = const TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 32,
    color: Colors.black,
    fontFamily: 'Nexa');

var indigoItalicTextStyle = const TextStyle(
    fontWeight: FontWeight.normal,
    fontSize: 20,
    color: Colors.indigo,
    fontFamily: 'Roboto');

var whiteTextStyle = const TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 32,
    color: Colors.white,
    fontFamily: 'Nexa');